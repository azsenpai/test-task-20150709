<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use app\models\Article;
use app\models\Comment;

class ArticleController extends Controller
{
	public function actionIndex()
	{
		$this->redirect('article/create');
	}

	/**
	 * Контреллер для создание статьи
	 */
	public function actionCreate()
	{
		if (Yii::$app->user->isGuest || Yii::$app->user->identity->getRid() != 1) {
			throw new NotFoundHttpException('Страница не найдена');
		}

		$model = new Article();
		$model->scenario = Article::SCENARIO_CREATE;

		return $this->render('create', ['model' => $model]);
	}

	/**
	 * Контреллер для сохранение статьи
	 *
	 * @return JSON
	 * @throws NotFoundHttpException
	 */
	public function actionSave()
	{
		if (Yii::$app->user->isGuest || Yii::$app->user->identity->getRid() != 1) {
			throw new NotFoundHttpException('Страница не найдена');
		}

		if (Yii::$app->request->getIsPost() && Yii::$app->request->getIsAjax()) {
			$model = new Article();
			$model->scenario = Article::SCENARIO_CREATE;

			$uid = Yii::$app->user->getId();
			$model->uid = is_null($uid) ? 0 : $uid;

			if ($model->load(Yii::$app->request->post()) && $model->save()) {
				$result = ['status' => 'ok'];
			} else {
				$result = ['status' => 'error'];
			}
		} else {
			throw new NotFoundHttpException('Страница не найдена');
		}

		Yii::$app->response->format = Response::FORMAT_JSON;

		return $result;
	}

	/**
	 * Контреллер для редактирование статьи
	 */
	public function actionEdit($id = 0)
	{
		if (Yii::$app->user->isGuest || Yii::$app->user->identity->getRid() != 1) {
			throw new NotFoundHttpException('Страница не найдена');
		}

		$id = (int)$id;

		$model = Article::findOne($id);
		$model->scenario = Article::SCENARIO_EDIT;

		if (is_null($model)) {
			throw new NotFoundHttpException('Страница не найдена');
		}

		return $this->render('edit', ['model' => $model]);
	}

	/**
	 * Контреллер для сохранение редактированной статьи
	 *
	 * @return JSON
	 * @throws NotFoundHttpException
	 */
	public function actionUpdate()
	{
		if (Yii::$app->user->isGuest || Yii::$app->user->identity->getRid() != 1) {
			throw new NotFoundHttpException('Страница не найдена');
		}

		if (Yii::$app->request->getIsPost() && Yii::$app->request->getIsAjax()) {
			$id = (int)Yii::$app->request->post('article_id');

			$model = Article::findOne($id);
			$model->scenario = Article::SCENARIO_EDIT;

			if (is_null($model)) {
				throw new NotFoundHttpException('Страница не найдена');
			}

			$uid = Yii::$app->user->getId();
			$model->uid = is_null($uid) ? 0 : $uid;

			if ($model->load(Yii::$app->request->post()) && $model->save()) {
				$result = ['status' => 'ok'];
			} else {
				$result = ['status' => 'error'];
			}
		} else {
			throw new NotFoundHttpException('Страница не найдена');
		}

		Yii::$app->response->format = Response::FORMAT_JSON;

		return $result;
	}

	/**
	 * Удаление статьи по id
	 *
	 * @return JSON
	 * @throws NotFoundHttpException
	 */
	public function actionDelete()
	{
		if (Yii::$app->user->isGuest || Yii::$app->user->identity->getRid() != 1) {
			throw new NotFoundHttpException('Страница не найдена');
		}

		if (Yii::$app->request->getIsPost() && Yii::$app->request->getIsAjax()) {
			$id = (int)Yii::$app->request->post('article_id');
			$model = Article::findOne($id);

			if (is_null($model)) {
				throw new NotFoundHttpException('Страница не найдена');
			}

			try {
				$model->delete();
				$result = ['status' => 'ok'];
			}
			catch (Exception $e) {
				$result = [
					'status' => 'error',
					'message' => $e->getMessage(),
				];
			}
		} else {
			throw new NotFoundHttpException('Страница не найдена');
		}

		Yii::$app->response->format = Response::FORMAT_JSON;

		return $result;
	}

	/**
	 * Контреллер для вывода статьи
	 */
	public function actionShow($id = 0)
	{
		$id = (int)$id;
		$model = Article::findOne($id);

		if (is_null($model)) {
			throw new NotFoundHttpException('Страница не найдена');
		}

		$model_comment = new Comment();

		$comments = Comment::find()
			->where(['aid' => $id])
			->orderBy('created DESC')
			->all();

		return $this->render('show', [
			'model' => $model,
			'model_comment' => $model_comment,
			'comments' => $comments,
		]);
	}

	public function actionCommentSave()
	{
		if (Yii::$app->request->getIsPost() && Yii::$app->request->getIsAjax()) {
			$aid = (int)Yii::$app->request->post('aid');

			$model = new Comment();
			$model->aid = $aid;
			
			if ($model->load(Yii::$app->request->post()) && $model->save()) {
				$result = ['status' => 'ok'];
			} else {
				$result = [
					'status' => 'error',
					'errors' => $model->errors,
				];
			}
		} else {
			throw new NotFoundHttpException('Страница не найдена');
		}

		Yii::$app->response->format = Response::FORMAT_JSON;

		return $result;
	}
}
