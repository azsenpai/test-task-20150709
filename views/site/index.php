<?php

/* @var $this yii\web\View */

use yii\helpers\Url;

$this->title = 'Главная | Alina.dev';
$this->registerJsFile(Url::base() . 'js/site/index.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<div class="site-index">
    <div class="body-content">
    	<ul class="articles list-unstyled">
    		<?php foreach ($articles as $article): ?>
    			<li>
    				<div class="container">
	    				<div class="col-lg-8">
	    					<a class="title" href="<?= '/article/show?id=' . $article->id ?>"><?= $article->title ?></a>
	    				</div>
	    				<div class="col-lg-4">
		    				<div class="btn-group">
		    					<a href="<?= '/article/show?id=' . $article->id ?>" class="btn btn-success btn-sm">Подробнее</a>
		    					<?php if (!Yii::$app->user->isGuest && Yii::$app->user->identity->getRid() == 1): ?>
			    					<a href="<?= '/article/edit?id=' . $article->id ?>" class="btn btn-primary btn-sm">Редактировать</a>
			    					<a href="#" class="btn btn-warning btn-sm remove" data-article_id="<?= $article->id ?>">Удалить</a>
		    					<?php endif; ?>
		    				</div>
	    				</div>
    				</div>
    			</li>
    		<?php endforeach ?>
    	</ul>
    </div>
</div>
