<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = $model->title;
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile(Url::base() . 'js/article/show.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<div class="article-show">
    <h1><?= $model->title ?></h1>
    <div><?= $model->body ?></div>
    <hr>
    <h3>Написать комментарии</h3>
    <?php $form = ActiveForm::begin([
        'id' => 'create-comment',
        'action' => '/article/comment-save',
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-5\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-2 control-label'],
        ],
    ]); ?>

        <?= $form->field($model_comment, 'email') ?>

        <?= $form->field($model_comment, 'comment')->textArea(['rows' => 6]) ?>

        <?= $form->field($model_comment, 'verifyCode')->widget(Captcha::className(), [
            'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
        ]) ?>


        <input type="hidden" name="aid" value="<?= $model->id; ?>">

        <div class="form-group">
            <div class="col-lg-offset-2 col-lg-10">
                <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary', 'name' => 'send-button']) ?>
            </div>
        </div>
        <div class="form-group">
        	<div class="col-lg-offset-2 col-lg-10 result"></div>
        </div>

    <?php ActiveForm::end(); ?>
    <hr>
    <h3>Комментарии</h3>
    <ul class="comments list-unstyled">
    	<?php foreach ($comments as $comment): ?>
	    	<li>
				<div><?= Html::encode($comment->email) ?></div>
				<div class="comment"><?= Html::encode($comment->comment) ?></div>
	    	</li>
    	<?php endforeach; ?>
    </ul>
</div>
