<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = 'Редактирование статьи';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile(Url::base() . 'js/article/edit.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<div class="article-edit">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin([
        'id' => 'edit-article',
        'action' => '/article/update',
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-5\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-2 control-label'],
        ],
    ]); ?>

        <?= $form->field($model, 'title') ?>

        <?= $form->field($model, 'body')->textArea(['rows' => 10]) ?>

        <?= $form->field($model, 'created') ?>

        <input type="hidden" name="article_id" value="<?= $model->id; ?>">

        <div class="form-group">
            <div class="col-lg-offset-2 col-lg-10">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name' => 'save-button']) ?>
            </div>
        </div>
        <div class="form-group">
        	<div class="col-lg-offset-2 col-lg-10 result"></div>
        </div>

    <?php ActiveForm::end(); ?>
</div>
