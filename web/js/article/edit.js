jQuery(function($) {
	var $form = $('#edit-article');
	var $submit = $form.find('[name="save-button"]');
	var $result = $form.find('.result');

	$form.on('beforeSubmit', function(e) {
		if ($form.find('.has-error').length > 0 || $submit.hasClass('disabled')) {
			return false;
		} else {
			submitForm();
		}

		return false;
	});

	function submitForm() {
		$submit.addClass('disabled');

		$result.html('Пожалуйста подождите ....');
		$result.slideDown();

		$.ajax({
			url: $form.attr('action'),
			type: $form.attr('method'),
			data: $form.serialize(),
			dataType: 'json',
			success: function(data) {
				if ('status' in data) {
					if (data.status == 'ok') {
						$result.html('Статья успешно сохранена');
					} else {
						$result.html('Произошла ошибка при сохранении статьи');
					}
				}
			},
			error: function() {
			},
			complete: function() {
				$submit.removeClass('disabled');
			}
		});
	}
});
