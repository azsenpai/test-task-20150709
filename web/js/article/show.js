jQuery(function($) {
	var $form = $('#create-comment');
	var $submit = $form.find('[name="send-button"]');
	var $result = $form.find('.result');

	var $comments = $('.comments');

	$form.on('beforeSubmit', function(e) {
		if ($form.find('.has-error').length > 0 || $submit.hasClass('disabled')) {
			return false;
		} else {
			submitForm();
		}

		return false;
	});

	function submitForm() {
		$submit.addClass('disabled');

		$result.html('Пожалуйста подождите ....');
		$result.slideDown();

		var email = $form.find('#comment-email').val();
		var comment = $form.find('#comment-comment').val();

		$.ajax({
			url: $form.attr('action'),
			type: $form.attr('method'),
			data: $form.serialize(),
			dataType: 'json',
			success: function(data) {
				if ('status' in data) {
					if (data.status == 'ok') {
						$result.html('Комментарии успешно добавлен');
						var html = '';

				    	html += '<li>';
						html += '<div>' + email + '</div>';
						html += '<div class="comment">' + comment + '</div>';
				    	html += '</li>';

				    	$comments.prepend(html);

				    	$('html, body').animate({ scrollTop: $comments.position().top });
					} else {
						$result.html('Произошла ошибка при добавлении комментария');
					}
					$('#comment-verifycode-image').yiiCaptcha('refresh');
				}
			},
			error: function() {
			},
			complete: function() {
				$submit.removeClass('disabled');
			}
		});
	}
});
