jQuery(function($) {
	$('.articles .remove').on('click', function(e) {
	    e.preventDefault();
		
		var $this = $(this);

		if ($this.hasClass('disabled')) {
			return;
		}

		$this.addClass('disabled');

		$.ajax({
			url: '/article/delete',
			type: 'post',
			data: {
				article_id: $this.data('article_id')
			},
			dataType: 'json',
			success: function(data) {
				if ('status' in data) {
					if (data.status == 'ok') {
						$this.parents('li').remove();
					}
				}
			},
			error: function() {
			},
			complete: function() {
				$this.removeClass('disabled');
			}
		});
	});
});