<?php

namespace app\models;

use yii\db\ActiveRecord;

class Article extends ActiveRecord
{
	const SCENARIO_CREATE = 'create';
	const SCENARIO_EDIT = 'edit';

	public static function tableName()
	{
		return 'articles';
	}

	public function scenarios()
	{
		$scenarios = parent::scenarios();

		$scenarios[self::SCENARIO_CREATE] = ['title', 'body'];
		$scenarios[self::SCENARIO_EDIT] = ['title', 'body', 'created'];

		return $scenarios;
	}

    public function attributeLabels()
    {
		return [
			'title' => 'Заголовок',
			'body' => 'Тело',
			'created' => 'Время создание',
		];
    }

    public function rules()
    {
        return [
            [['title', 'body'], 'required', 'on' => self::SCENARIO_CREATE],
            [['title', 'body', 'created'], 'required', 'on' => self::SCENARIO_EDIT],
        ];
    }
}
