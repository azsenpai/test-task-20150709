<?php

namespace app\models;

use yii\db\ActiveRecord;

class Comment extends ActiveRecord
{
	public $verifyCode;

	public static function tableName()
	{
		return 'comments';
	}

    public function attributeLabels()
    {
		return [
			'email' => 'E-mail',
			'comment' => 'Комментарии',
			'verifyCode' => 'Код с картинки'
		];
    }

    public function rules()
    {
        return [
            [['email', 'comment', 'verifyCode'], 'required'],
            ['email', 'email'],
            ['verifyCode', 'captcha'],
        ];
    }
}
